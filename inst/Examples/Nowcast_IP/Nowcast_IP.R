library(jsonlite)
library(data.table)
library(BDFM)

setwd("C:/Users/Seth/Documents/BDFM/inst/Examples")
source("./Nowcast_IP/helper_functions.R")

unique_names <- c('INDPRO', #1 Industrial Production Index, monthly, level
                  'USSLIND', #2 US Leading Index --- this will be adjusted forward one period
                  'UNRATE', #3 Unemployment, monthly
                  'FRGSHPUSM649NCIS', #4 CASS freight index, level, not SA --- comes out one day ahead
                  'CPILFESL', #5 CPI
                  'ICSA', #6 Initial claims, SA, weekly
                  'WILL5000IND', #7 Willshire 5000, daily, level
                  'TWEXB', #8 exchange rate index, weekly  
                  'T10Y3M') #9 10Y to 3M treasury spread, daily

Type <- c('Production', #1 Industrial Production
          'Survey', #2 US Leading Index
          'Employment', #3 Employment
          'Trade', #4 CASS freight index
          'Prices', #5 CPI
          'Employment', #6 Initial Claims
          'Prices', #7 Willshire 5000
          'Prices', #8 Exchange Rate
          'Prices') #9 treasury spread



Get_Data <- function(series_name){
  series_name = unique_names[6]
    Data <- get_fred_data(series_id = series_name, api_key = "9c39f8a4fbcc38c22475bc7f26988367", output_type = "2", observation_start = "1980-01-01", observation_end = "2018-09-30", frequency = "m",  vintage_dates = "2018-09-30")
  return(Data)
}   

RawData <- lapply(unique_names, FUN = Get_Data) #get data from the Fred API
Data    <- rbindlist(RawData) #stack observations into a data.table
dt      <- call_data(unique_names, dt = Data, series_id = "series_name") #put data into matrix format
dt      <- setcolorder(dt, c("date", unique_names)) #make sure columns are in the right order
dates   <- dt$date #put dates into a date vector
Y       <- as.matrix(dt[,-1, with = F]) #data matrix that will enter BDFM()


