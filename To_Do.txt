Format Output:
	- Output factors as X, not stacked factors as Z
    Optional Output:
	- Update contributions at each period
        - Posterior distributions of predictions? This gets to be very big quickly!

Input priors as a fraction of observations, as opposed to raw number of "prior observations"

Preserve row and column names of input data